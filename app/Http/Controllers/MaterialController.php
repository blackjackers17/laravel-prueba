<?php

namespace App\Http\Controllers;

use App\Models\Material;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Resources\MaterialResource;

class MaterialController extends Controller
{
    public function index()
    {
        $materials = Material::all();
        return $this->success_response(
            MaterialResource::collection($materials),
            Response::HTTP_OK,
            [
                'headers'   => MaterialResource::headers()
            ]
        );
    }

    public function store(Request $request)
    {
        $request->validate([
            'code' => 'required|unique:materials',
            'description' => 'required|unique:materials',
            'unit' => 'required|in:m2,und,kg',
            'price' => 'required|numeric',
        ]);

        $material = Material::create($request->all());
        return $this->success_response(
            new MaterialResource($material),
            Response::HTTP_OK
        );
    }

    public function show(Material $material)
    {
        return $this->success_response(
            new MaterialResource($material),
            Response::HTTP_OK
        );
    }

    public function update(Request $request, Material $material)
    {
        $request->validate([
            'code' => 'required|unique:materials,code,' . $material->id,
            'description' => 'required',
            'unit' => 'required|in:m2,und,kg',
            'price' => 'required|numeric',
        ]);

        $material->update($request->all());
        return $this->success_response(
            new MaterialResource($material),
            Response::HTTP_OK
        );
    }

    public function destroy(Material $material)
    {
        $material->delete();
        return response()->noContent();
    }
}
