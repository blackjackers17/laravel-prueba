<?php

namespace App\Http\Controllers;

use App\Models\MaterialProject;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\Rule;
use App\Resources\MaterialProjectResource;

class MaterialProjectController extends Controller
{
    public function index()
    {
        $data = MaterialProject::with('material','project')->get();
        return $this->success_response(
            MaterialProjectResource::collection($data),
            Response::HTTP_OK,
            [
                'headers'   => MaterialProjectResource::headers()
            ]
        );
    }

    public function store(Request $request)
    {
        $request->validate([
            'material_id' => [
                'required',
                'exists:materials,id',
                Rule::unique('material_project')->where(function ($query) use ($request) {
                    return $query->where('project_id', $request->project_id);
                }),
            ],
            'project_id' => 'required|exists:projects,id',
            'quantity' => 'required|integer',
        ],[
            'material_id.unique' => 'El material ya está asociado al proyecto.',
        ]);
        $item = MaterialProject::create($request->all());
        return $this->success_response(
            new MaterialProjectResource($item->load('material','project')),
            Response::HTTP_OK
        );
    }

    public function show(MaterialProject $materialProject)
    {
        return $this->success_response(
            new MaterialProjectResource($materialProject->load('material','project')),
            Response::HTTP_OK
        );
    }

    public function update(Request $request, MaterialProject $materialProject)
    {
        $request->validate([
            'material_id' => [
                'required',
                'exists:materials,id',
                Rule::unique('material_project')->ignore($materialProject->id)->where(function ($query) use ($request) {
                    return $query->where('project_id', $request->project_id);
                }),
            ],
            'project_id' => 'required|exists:projects,id',
            'quantity' => 'required|integer',
        ], [
            'material_id.unique' => 'El material ya está asociado al proyecto.'
        ]);
        $materialProject->update($request->all());
        return $this->success_response(
            new MaterialProjectResource($materialProject->load('material','project')),
            Response::HTTP_OK
        );
    }

    public function destroy(MaterialProject $materialProject)
    {
        $materialProject->delete();
        return response()->noContent();
    }

    public function report()
    {
        $projects = MaterialProject::with(['project', 'material'])->get()->groupBy('project_id');
        $report = [];
        foreach ($projects as $projectId => $materials) {
            $projectName = $materials->first()->project->name;
            $report[$projectName] = [
                'materials' => $materials->map(function ($materialProject) {
                    return [
                        'description' => $materialProject->material->description,
                        'quantity' => $materialProject->quantity,
                        'price' => $materialProject->material->price,
                        'total' => $materialProject->material->price * $materialProject->quantity
                    ];
                })->toArray(),
            ];
        }
        return response()->json($report);
    }
}
