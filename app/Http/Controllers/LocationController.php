<?php

namespace App\Http\Controllers;

use App\Models\Country;
use App\Models\Department;
use App\Models\City;

class LocationController extends Controller
{
    public function getCountries()
    {
        $countries = Country::all();
        return response()->json($countries);
    }

    public function getDepartmentsByCountry($countryId)
    {
        $departments = Department::where('country_id', $countryId)->get();
        return response()->json($departments);
    }

    public function getCitiesByDepartment($departmentId)
    {
        $cities = City::where('department_id', $departmentId)->get();
        return response()->json($cities);
    }
}
