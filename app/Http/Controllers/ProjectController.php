<?php

namespace App\Http\Controllers;

use App\Models\Project;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Resources\ProjectResource;

class ProjectController extends Controller
{
    public function index()
    {
        $projects = Project::with('department', 'city')->get();
        return $this->success_response(
            ProjectResource::collection($projects),
            Response::HTTP_OK,
            [
                'headers'   => ProjectResource::headers()
            ]
        );
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:projects',
            'department_id' => 'required',
            'city_id' => 'required',
            'country_id' => 'required'
        ]);
        $project = Project::create($request->all());
        return $this->success_response(
            new ProjectResource($project->load('department', 'city')),
            Response::HTTP_OK
        );
    }

    public function show(Project $project)
    {
        return $this->success_response(
            new ProjectResource($project->load('department', 'city')),
            Response::HTTP_OK
        );
    }

    public function update(Request $request, Project $project)
    {
        $request->validate([
            'name' => 'required',
            'department_id' => 'required',
            'city_id' => 'required',
            'country_id' => 'required'
        ]);
        $project->update($request->all());
        return $this->success_response(
            new ProjectResource($project->load('department', 'city')),
            Response::HTTP_OK
        );
    }

    public function destroy(Project $project)
    {
        $project->delete();
        return response()->noContent();
    }
}
