<?php

namespace App\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MaterialResource extends JsonResource
{

    public function __construct($resource)
    {
        parent::__construct($resource);
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => isset($this->id) ? (int) $this->id : null,
            'code' => isset($this->code) ? $this->code : null,
            'description' => isset($this->description) ? $this->description : null,
            'unit' => isset($this->unit) ? $this->unit : null,
            'price' => isset($this->price) ? $this->price : null
        ];
    }

    public static function headers()
    {
        return [
            [
                'text' => "Código",
                'value'  =>  "code"
            ],
            [
                'text' => "Descripción",
                'value'  =>  "description"
            ],
            [
                'text' => "Unidad",
                'value'  =>  "unit"
            ],
            [
                'text' => "Precio",
                'value'  =>  "price"
            ],
            [
                'text' => "Acciones",
                'value'  =>  "actions"
            ]
        ];
    }
}
