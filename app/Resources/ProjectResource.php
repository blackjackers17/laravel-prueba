<?php

namespace App\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProjectResource extends JsonResource
{

    public function __construct($resource)
    {
        parent::__construct($resource);
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => isset($this->id) ? (int) $this->id : null,
            'name' => isset($this->name) ? $this->name : null,
            'country_id' => isset($this->country_id) ? (int)$this->country_id : null,
            'country' => isset($this->country->name) ? $this->country->name : null,
            'department_id' => isset($this->department_id) ? (int)$this->department_id : null,
            'department' => isset($this->department->name) ? $this->department->name : null,
            'city_id' => isset($this->city_id) ? (int)$this->city_id : null,
            'city' => isset($this->city->name) ? $this->city->name : null
        ];
    }

    public static function headers()
    {
        return [
            [
                'text' => "Nombre",
                'value'  =>  "name"
            ],
            [
                'text' => "País",
                'value'  =>  "country"
            ],
            [
                'text' => "Departamento",
                'value'  =>  "department"
            ],
            [
                'text' => "Ciudad",
                'value'  =>  "city"
            ],
            [
                'text' => "Acciones",
                'value'  =>  "actions"
            ]
        ];
    }
}
