<?php

namespace App\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MaterialProjectResource extends JsonResource
{

    public function __construct($resource)
    {
        parent::__construct($resource);
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => isset($this->id) ? (int) $this->id : null,
            'material_id' => isset($this->material_id) ? $this->material_id : null,
            'material' => isset($this->material) ? $this->material->description : null,
            'project_id' => isset($this->project_id) ? $this->project_id : null,
            'project' => isset($this->project) ? $this->project->name : null,
            'quantity' => isset($this->quantity) ? $this->quantity : null
        ];
    }

    public static function headers()
    {
        return [
            [
                'text' => "Proyecto",
                'value'  =>  "project"
            ],
            [
                'text' => "Material",
                'value'  =>  "material"
            ],
            [
                'text' => "Cantidad",
                'value'  =>  "quantity"
            ],
            [
                'text' => "Acciones",
                'value'  =>  "actions"
            ]
        ];
    }
}
