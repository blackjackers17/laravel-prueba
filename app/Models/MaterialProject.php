<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class MaterialProject extends Pivot
{
    protected $table = 'material_project';

    protected $fillable = ['material_id', 'project_id', 'quantity'];

    public function material()
    {
        return $this->belongsTo(Material::class);
    }

    public function project()
    {
        return $this->belongsTo(Project::class);
    }   
}
