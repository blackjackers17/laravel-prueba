<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Material extends Model
{
    use HasFactory;

    protected $fillable = ['code', 'description', 'unit', 'price'];

    public function projects()
    {
        return $this->belongsToMany(Project::class)->withPivot('quantity')->withTimestamps();
    }

    public function materialProjects()
    {
        return $this->hasMany(MaterialProject::class);
    }
}
