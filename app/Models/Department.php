<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'city', 'country_id'];

    public function country()
    {
        return $this->belongsTo(Country::class);
    }
}
