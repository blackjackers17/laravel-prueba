<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CitiesTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('cities')->insert([
            ['name' => 'Leticia', 'department_id' => 1],
            ['name' => 'Puerto Nariño', 'department_id' => 1],
            ['name' => 'La Chorrera', 'department_id' => 1],
            ['name' => 'Medellín', 'department_id' => 2],
            ['name' => 'Bello', 'department_id' => 2],
            ['name' => 'Envigado', 'department_id' => 2],
            ['name' => 'Saravena', 'department_id' => 3],
            ['name' => 'Tame', 'department_id' => 3],
            ['name' => 'Tunja', 'department_id' => 6],
            ['name' => 'Duitama', 'department_id' => 6],
            ['name' => 'Sogamoso', 'department_id' => 6],
            ['name' => 'Paipa', 'department_id' => 6],
            ['name' => 'Madrid', 'department_id' => 14],
            ['name' => 'Facatativá', 'department_id' => 14],
            ['name' => 'Funza', 'department_id' => 14],
            ['name' => 'Mosquera', 'department_id' => 14],
            // Add more cities as needed
        ]);
    }
}
