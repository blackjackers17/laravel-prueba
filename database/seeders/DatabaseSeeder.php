<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call([
            CountriesTableSeeder::class,
            DepartmentsTableSeeder::class,
            CitiesTableSeeder::class
        ]);
    }
}
