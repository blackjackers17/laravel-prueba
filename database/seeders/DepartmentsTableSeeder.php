<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DepartmentsTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('departments')->insert([
            ['name' => 'Amazonas', 'city' => 'Leticia', 'country_id' => 1],
            ['name' => 'Antioquia', 'city' => 'Medellín', 'country_id' => 1],
            ['name' => 'Arauca', 'city' => 'Arauca', 'country_id' => 1],
            ['name' => 'Atlántico', 'city' => 'Barranquilla', 'country_id' => 1],
            ['name' => 'Bolívar', 'city' => 'Cartagena', 'country_id' => 1],
            ['name' => 'Boyacá', 'city' => 'Tunja', 'country_id' => 1],
            ['name' => 'Caldas', 'city' => 'Manizales', 'country_id' => 1],
            ['name' => 'Caquetá', 'city' => 'Florencia', 'country_id' => 1],
            ['name' => 'Casanare', 'city' => 'Yopal', 'country_id' => 1],
            ['name' => 'Cauca', 'city' => 'Popayán', 'country_id' => 1],
            ['name' => 'Cesar', 'city' => 'Valledupar', 'country_id' => 1],
            ['name' => 'Chocó', 'city' => 'Quibdó', 'country_id' => 1],
            ['name' => 'Córdoba', 'city' => 'Montería', 'country_id' => 1],
            ['name' => 'Cundinamarca', 'city' => 'Bogotá', 'country_id' => 1],
            ['name' => 'Guainía', 'city' => 'Inírida', 'country_id' => 1],
            ['name' => 'Guaviare', 'city' => 'San José del Guaviare', 'country_id' => 1],
            ['name' => 'Huila', 'city' => 'Neiva', 'country_id' => 1],
            ['name' => 'La Guajira', 'city' => 'Riohacha', 'country_id' => 1],
            ['name' => 'Magdalena', 'city' => 'Santa Marta', 'country_id' => 1],
            ['name' => 'Meta', 'city' => 'Villavicencio', 'country_id' => 1],
            ['name' => 'Nariño', 'city' => 'Pasto', 'country_id' => 1],
            ['name' => 'Norte de Santander', 'city' => 'Cúcuta', 'country_id' => 1],
            ['name' => 'Putumayo', 'city' => 'Mocoa', 'country_id' => 1],
            ['name' => 'Quindío', 'city' => 'Armenia', 'country_id' => 1],
            ['name' => 'Risaralda', 'city' => 'Pereira', 'country_id' => 1],
            ['name' => 'San Andrés y Providencia', 'city' => 'San Andrés', 'country_id' => 1],
            ['name' => 'Santander', 'city' => 'Bucaramanga', 'country_id' => 1],
            ['name' => 'Sucre', 'city' => 'Sincelejo', 'country_id' => 1],
            ['name' => 'Tolima', 'city' => 'Ibagué', 'country_id' => 1],
            ['name' => 'Valle del Cauca', 'city' => 'Cali', 'country_id' => 1],
            ['name' => 'Vaupés', 'city' => 'Mitú', 'country_id' => 1],
            ['name' => 'Vichada', 'city' => 'Puerto Carreño', 'country_id' => 1],
        ]);
    }
}
