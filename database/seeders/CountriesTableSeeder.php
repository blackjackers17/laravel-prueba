<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CountriesTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('countries')->insert([
            ['name' => 'Colombia']
            // Agrega más países según sea necesario
        ]);
    }
}
