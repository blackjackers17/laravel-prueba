<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MaterialController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\MaterialProjectController;
use App\Http\Controllers\LocationController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiResource('materials', MaterialController::class);
Route::apiResource('projects', ProjectController::class);
Route::apiResource('material-projects', MaterialProjectController::class);

Route::get('countries', [LocationController::class, 'getCountries']);
Route::get('departments/{countryId}', [LocationController::class, 'getDepartmentsByCountry']);
Route::get('cities/{departmentId}', [LocationController::class, 'getCitiesByDepartment']);

Route::get('/report', [MaterialProjectController::class, 'report']);
